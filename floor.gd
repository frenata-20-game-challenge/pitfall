extends TileMap

@export var hole_probability = 0.01

func _ready():
	var x = 0
	while x < 73:
		if randf() < hole_probability and x > 10 and x < 63:
			# make a hole
			for i in range(4):
				set_cell(0, Vector2(x+i, 0), 0, Vector2i(0,3))
			x += 4
		else:
			set_cell(0, Vector2(x, 0), 0, Vector2i(0,2))
			x += 1
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
