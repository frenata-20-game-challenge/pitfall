extends Node2D

signal exit(id)

var id: int

func new(id):
	self.id = id
	return self

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func exit_right(body):
	# NOTE: go forward a level
	exit.emit(id+1)
	queue_free()

func exit_left(body):
	# NOTE: go back a level
	exit.emit(id-1)
	queue_free()
