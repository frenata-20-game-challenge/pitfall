extends Node

var level_scene = preload("res://level.tscn")


func _ready():
	new_level(1)


func new_level(id):
	var level = level_scene.instantiate().new(1)
	call_deferred("add_child", level)
	level.exit.connect(new_level)
